class person:
    def __init__(self,first_name,last_name,age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
    def full_name(self):
        return ("{} {}".format(self.first_name,self.last_name))
    def is_above(self):
        return self.age>18

p1 = person('ashish','yadav',17)
p2 = person('mohit','kumar', 28)

#print(p1.first_name)
#print(p2.first_name)
print(p1.full_name())
print(p1.is_above())
