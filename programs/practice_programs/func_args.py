"""def func_args(*args):
    total=0
    for i in args:
        total+=i
    return total
print(func_args(1,2))"""

"""def weight(*args, g=9.8):
    for i in args:
        print("{}={}".format(i,i*g))
weight(10,11,12)"""

"""def mul(a,*args):
    multiply=1
    print(a)
    print(type(args))
    for i in args:
        multiply*=i
    return multiply
print(mul(10,20,2))"""

def func(Fname,**kwargs):
    for i,j in kwargs.items():
        print("{}={}".format(i,j))
    print(Fname)
func("Vika",name='Mohit',age=25)

"""def func(name,*args,last_name='unknown',**kwargs):
    print(name)
    print(args)
    print(last_name)
    print(kwargs)
func('ashish',1,2,3,"yadav",a=1,b=1)"""

